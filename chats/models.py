from django.db import models
from django.contrib.auth import get_user_model
from django.db.models import Q

User = get_user_model()


class Conversation(models.Model):
    id = models.CharField(max_length=45, primary_key=True, unique=True, blank=True)
    user1 = models.ForeignKey(User, on_delete=models.PROTECT, related_name='user1')
    user2 = models.ForeignKey(User, on_delete=models.PROTECT, related_name='user2')
    deleted_by_user1 = models.BooleanField(default=False)
    deleted_by_user2 = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.id

    @classmethod
    def get_conversation_by_username(cls, username):
        return Conversation.objects.filter(
            Q(user1__username__startswith=username) | Q(user2__username__endswith=username)
        ).order_by('-updated')

    @classmethod
    def generate_id(cls, user1, user2):
        return '<->'.join(sorted([user1.username, user2.username]))

    def mark_as_read(self, user):
        return self.messages.filter(read=False).exclude(sender=user).update(received=True, read=True)

    def save(self, *args, **kwargs):
        self.id = Conversation.generate_id(self.user1, self.user2)
        return super().save(*args, **kwargs)


class Message(models.Model):
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE, related_name='messages')
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent')
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received')
    content = models.TextField(max_length=1000)
    timestamp = models.DateTimeField(auto_now_add=True)
    sent = models.BooleanField(default=True)
    received = models.BooleanField(default=False)
    read = models.BooleanField(default=False)
    updated = models.DateTimeField(auto_now=True)
    deleted_by_sender = models.BooleanField(default=False)
    deleted_by_receiver = models.BooleanField(default=False)

    def __str__(self):
        return self.sender.username + ' -> ' + self.receiver.username

    def save(self, *args, **kwargs):
        if not self.pk:
            self.conversation.updated = self.timestamp
            self.conversation.save()

        return super().save(*args, **kwargs)


