from django.urls import path

from .views import ProfileListView, ProfileCreateView

urlpatterns = [
    path('', ProfileListView.as_view()),
    path('new/', ProfileCreateView.as_view()),
]