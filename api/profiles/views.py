from rest_framework.generics import ListAPIView, CreateAPIView

from .serializers import ProfileSerializer
from profiles.models import Profile


class ProfileListView(ListAPIView):
    serializer_class = ProfileSerializer
    queryset = Profile.objects.all()


class ProfileCreateView(CreateAPIView):
    serializer_class = ProfileSerializer