from rest_framework.serializers import ModelSerializer

from profiles.models import Profile

class ProfileSerializer(ModelSerializer):
    class Meta:
        model = Profile
        exclude = ('user',)

    def create(self, validated_data):
        user = self.context.get('request').user
        profile = Profile(user=user, **validated_data)
        profile.save()
        return profile


class ProfileBasicSerializer(ModelSerializer):
    class Meta:
        model = Profile
        fields = ('full_name',)
