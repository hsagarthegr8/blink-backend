from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import get_user_model
from django.db.models import Q

from .serializers import UserSerializer, UserBasicSerializer
from .utils import generate_token


User = get_user_model()


class UserListView(ListAPIView):
    serializer_class = UserBasicSerializer

    def get_queryset(self, *args, **kwargs):
        q = self.request.query_params.get('q')
        try:
            if q:
                return User.objects.filter(profile__full_name__contains=q).exclude(
                    Q(username=self.request.user) | Q(profile__isnull=True)
                )
            raise ("Invalid Query")
        except:
            return []


class UserCreateView(CreateAPIView):
    permission_classes = ''
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token = generate_token(user)
        headers = self.get_success_headers(serializer.data)
        response = {**serializer.data, 'token': token}
        return Response(response, status=status.HTTP_201_CREATED, headers=headers)


class UserDetailView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
