from rest_framework import serializers
from django.contrib.auth import get_user_model



User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    dateJoined = serializers.DateField(source='date_joined', read_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'dateJoined', 'password')
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        user = User(username=validated_data['username'], email=validated_data['email'])
        user.set_password(validated_data['password'])
        user.save()
        
        return user


class UserBasicSerializer(serializers.ModelSerializer):
    fullName = serializers.CharField(source='profile.full_name')
    image_url = serializers.URLField(source='profile.image_url')

    class Meta:
        model = User
        fields = ('username', 'fullName', 'image_url')

