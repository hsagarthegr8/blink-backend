from django.urls import path

from .views import UserListView, UserCreateView, UserDetailView


urlpatterns = [
    path('', UserListView.as_view()),
    path('new/', UserCreateView.as_view()),
    path('<str:pk>/', UserDetailView.as_view()),
]