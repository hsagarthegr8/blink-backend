from rest_framework.serializers import ModelSerializer, SerializerMethodField
from rest_framework.pagination import DjangoPaginator

from chats.models import Conversation, Message
from api.accounts.serializers import UserBasicSerializer


class MessageSerializer(ModelSerializer):
    class Meta:
        model = Message
        exclude = ('conversation',)
        extra_kwargs = {
            'read': {'read_only': True},
            'sent': {'read_only': True},
            'received': {'read_only': True},
            'deleted_by_sender': {'read_only': True},
            'deleted_by_receiver': {'read_only': True},
            'sender': {'read_only': True},
            'receiver': {'write_only': True}
        }

    def create(self, validated_data):
        sender = self.context['user']
        receiver = validated_data['receiver']
        conversation_id = Conversation.generate_id(sender, receiver)
        conversation = Conversation.objects.filter(pk=conversation_id)
        if conversation.exists():
            conversation = conversation.first()
        else:
            conversation = Conversation.objects.create(user1=sender, user2=receiver)
        message = Message(sender=sender, receiver=receiver, conversation=conversation, content=validated_data['content'])
        message.save()
        return message


class ConversationListSerializer(ModelSerializer):
    user1 = UserBasicSerializer(write_only=True)
    user2 = UserBasicSerializer(write_only=True)
    recipient = SerializerMethodField()
    message = SerializerMethodField()
    unread = SerializerMethodField()

    def get_recipient(self, obj):
        if self.context['user'] == obj.user1:
            user = obj.user2
        else:
            user = obj.user1
        user = UserBasicSerializer(user).data
        return user

    def get_message(self, obj):
        try:
            query = Message.objects.filter(conversation=obj).order_by('-timestamp')[0:1].get()
        except:
            return None
        serializer = MessageSerializer(query)
        return serializer.data

    def get_unread(self, obj):
        try:
            message = Message.objects.filter(conversation=obj).order_by('-timestamp')[0:1].get()
            user = self.context['user']
            return message.sender != user and not message.read
        except:
            return False

    class Meta:
        model = Conversation
        fields = ('id', 'recipient', 'user1', 'user2', 'message', 'unread', 'timestamp', 'updated')


class ConversationDetailSerializer(ModelSerializer):
    user1 = UserBasicSerializer(write_only=True)
    user2 = UserBasicSerializer(write_only=True)
    messages = SerializerMethodField()

    def get_messages(self, obj):
        try:
            query = Message.objects.filter(conversation=obj).order_by('timestamp')
        except:
            return None
        # page_no = self.context.get('page')
        # paginator = DjangoPaginator(query, 20)
        try:
            # serializer = MessageSerializer(paginator.page(page_no), many=True)
            serializer = MessageSerializer(query, many=True)
        except:
            return None
        return serializer.data

    class Meta:
        model = Conversation
        fields = ('id', 'user1', 'user2', 'messages', 'timestamp', 'updated')

