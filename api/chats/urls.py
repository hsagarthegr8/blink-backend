from django.urls import path

from .views import ConversationListView, MessageCreateView, ConversationDetailView



urlpatterns = [
    path('', ConversationListView.as_view()),
    path('<str:pk>/', ConversationDetailView.as_view()),
    path('message/new/', MessageCreateView.as_view()),
]