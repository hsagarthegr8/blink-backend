from channels.generic.websocket import JsonWebsocketConsumer
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer
from jwt.exceptions import InvalidTokenError
from rest_framework.exceptions import ValidationError
from asgiref.sync import async_to_sync
from django.contrib.auth import get_user_model
from django.utils import timezone
from chats.models import Conversation, Message
from api.chats.serializers import ConversationListSerializer, ConversationDetailSerializer, MessageSerializer
from api.accounts.serializers import UserBasicSerializer

User = get_user_model()


class ChatConsumer(JsonWebsocketConsumer):
    def connect(self):
        print('Connected')
        self.accept()

    def receive_json(self, content, **kwargs):
        print(content['type'])
        if content['type'] == 'AUTHORIZATION':
            self.authenticate(content)
            self.send_conversation_list(self.scope['user'])

        elif content['type'] == 'GET_CONVERSATION':
            self.send_conversation(content)
            self.send_conversation_list(self.scope['user'])

        elif content['type'] == 'SEND_MESSAGE':
            self.send_message_handler(content)

        elif content['type'] == 'SEARCH_USER':
            self.search_user_and_send(content)

    def disconnect(self, code):
        print("Closing")
        self.close()

    def authenticate(self, content):
        try:
            print("Authenticating")
            result = VerifyJSONWebTokenSerializer().validate(content)
            self.scope['user'] = result['user']
            self.username = self.scope['user'].username

            async_to_sync(self.channel_layer.group_add)(self.username, self.channel_name)
            print("Authenticated")

        except (KeyError, InvalidTokenError, ValidationError):
            print("Authentication Failed")
            self.close()

    def send_conversation_list(self, user):
        queryset = Conversation.get_conversation_by_username(user.username)
        conversations = ConversationListSerializer(queryset, many=True, context={'user': user})
        print("Sending conversation list")
        conversation_list_action = {'type': 'CONVERSATION_LIST', 'conversations': conversations.data}
        async_to_sync(self.channel_layer.group_send)(user.username, {
            'type': 'channel.message',
            'message': conversation_list_action
        })

    def send_conversation(self, content):
        id = content['conversation_id']
        conversation = Conversation.objects.get(id=id)
        conversation.messages.filter(read=False, receiver=self.scope['user']).update(read=True, updated=timezone.now())
        conversation_serialized = ConversationDetailSerializer(conversation,
                                                               context={'user': self.scope['user']}).data
        self.send_json({'type': 'CONVERSATION', 'conversation': conversation_serialized})

    def send_message_handler(self, content):
        users = content['conversation'].split('<->')
        receiver = users[1] if users[0] == self.username else users[0]
        content['receiver'] = receiver
        self.send_message(content)
        self.send_conversation_list(self.scope['user'])
        receiver = User.objects.get(username=content['receiver'])
        self.send_conversation_list(receiver)

    def send_message(self, content):
        serialized_message = MessageSerializer(data=content, context={'user': self.scope['user']})
        serialized_message.is_valid()
        serialized_message.save()
        message_json_action = {'type': 'RECEIVE_MESSAGE', 'conversation': content['conversation'],
                               'message': serialized_message.data}
        async_to_sync(self.channel_layer.group_send)(self.username, {
            'type': 'channel.message',
            'message': message_json_action
        })
        async_to_sync(self.channel_layer.group_send)(content['receiver'], {
            'type': 'channel.message',
            'message': message_json_action
        })

    def search_user_and_send(self, content):
        if content['query']:
            users = User.objects.filter(profile__full_name__icontains=content['query']).exclude(username=self.username)
            serialized_users = UserBasicSerializer(users, many=True)
            user_list = serialized_users.data
        else:
            user_list = []
        self.send_json({'type': 'USER_SEARCH_RESULT', 'users': user_list})

    def channel_message(self, event):
        self.send_json(event['message'])

