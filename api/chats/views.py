from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.db.models import Q

from chats.models import Conversation, Message
from .serializers import ConversationListSerializer, MessageSerializer, ConversationDetailSerializer


class ConversationListView(ListAPIView):
    serializer_class = ConversationListSerializer
        
    def get_queryset(self, *args, **kwargs):
        return Conversation.get_conversation_by_username(self.request.user.username)

    def get_serializer_context(self):
        return {
            'user': self.request.user
        }


class ConversationDetailView(RetrieveAPIView):
    serializer_class = ConversationDetailSerializer

    def get_queryset(self):
        return Conversation.get_conversation_by_username(self.request.user.username)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({
            # 'page': self.kwargs['page'],
            'user': self.request.user
        })
        return context

    def get(self, *args, **kwargs):
        conversation = self.get_object()
        user = self.request.user
        conversation.mark_as_read(user)
        return super().get(*args, **kwargs)


class MessageCreateView(CreateAPIView):
    serializer_class = MessageSerializer


@api_view(['PATCH'])
def set_messages_to_read(request, pk):
    Message.objects.filter(
        Q(conversation__id=pk) & Q(read=False)
    ).update(read=True, received=True)

    return Response('Done')





