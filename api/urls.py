from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

urlpatterns = [
    path('accounts/', include('api.accounts.urls')),
    path('profiles/', include('api.profiles.urls')),
    path('chats/', include('api.chats.urls')),
    path('get-token/', obtain_jwt_token),
    path('refresh-token/', refresh_jwt_token),
    path('verify-token/', verify_jwt_token),
]