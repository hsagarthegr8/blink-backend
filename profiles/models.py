from django.db import models

from django.contrib.auth import get_user_model

User = get_user_model()


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    first_name = models.CharField(max_length=40, blank=False, null=False)
    last_name = models.CharField(max_length=40, blank=True, null=True)
    full_name = models.CharField(max_length=90, blank=True, null=True)
    GENDER_CHOICES = (('M', 'Male'), ('F','Female'))
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    date_of_birth = models.DateField(verbose_name='Date of Birth')
    about = models.TextField(blank=True, null=True)
    image_url = models.URLField(blank=True, null=True)

    def __str__(self):
        return self.user.username

    def save(self, *args, **kwargs):
        self.full_name = self.generate_full_name()
        super().save(*args, **kwargs)

    def generate_full_name(self):
        if self.last_name:
            return self.first_name + ' ' + self.last_name
        return self.first_name
